<style>
.glitch {
  animation: glitchy 2s steps(100) infinite;
}

@keyframes glitchy {
  0% {
    text-shadow: 1px 0 0 red, -1px 0 0 blue;
  }
  1% {
    text-shadow: 1px 0 0 red, -1px 0 0 blue;
  }
  2% {
    text-shadow: 1px 0 0 red, -1px 0 0 blue;
  }
  3% {
    text-shadow: 1px 0 0 red, -1px 0 0 blue;
  }
  4% {
    text-shadow: 1px 0 0 red, -1px 0 0 blue;
  }
  5% {
    text-shadow: -1px 0 0 red, 1px 0 0 blue;
  }
  6% {
    text-shadow: -1px 0 0 red, 1px 0 0 blue;
  }
  7% {
    text-shadow: -1px 0 0 red, 1px 0 0 blue;
  }
  8% {
    text-shadow: -1px 0 0 red, 1px 0 0 blue;
  }
  9% {
    text-shadow: -1px 0 0 red, 1px 0 0 blue;
  }
  10% {
    text-shadow: -1px 0 0 red, 1px 0 0 blue;
  }
  11% {
    text-shadow: 0.5px 0 0 red, -0.5px 0 0 lime;
  }
  12% {
    text-shadow: 0.5px 0 0 red, -0.5px 0 0 lime;
  }
  13% {
    text-shadow: 0.5px 0 0 red, -0.5px 0 0 lime;
  }
  14% {
    text-shadow: 0.5px 0 0 red, -0.5px 0 0 lime;
  }
  15% {
    text-shadow: 0.5px 0 0 red, -0.5px 0 0 lime;
  }
  16% {
    text-shadow: -1px 0 0 red, 1px 0 0 lime;
  }
  17% {
    text-shadow: -1px 0 0 red, 1px 0 0 lime;
  }
  18% {
    text-shadow: -1px 0 0 red, 1px 0 0 lime;
  }
  19% {
    text-shadow: -1px 0 0 red, 1px 0 0 lime;
  }
  20% {
    text-shadow: -1px 0 0 red, 1px 0 0 lime;
  }
  21% {
    text-shadow: 0.7px 0 0 blue, -0.7px 0 0 lime;
  }
  22% {
    text-shadow: 0.7px 0 0 blue, -0.7px 0 0 lime;
  }
  23% {
    text-shadow: 0.7px 0 0 blue, -0.7px 0 0 lime;
  }
  24% {
    text-shadow: 0.7px 0 0 blue, -0.7px 0 0 lime;
  }
  25% {
    text-shadow: 0.7px 0 0 blue, -0.7px 0 0 lime;
  }
  26% {
    text-shadow: 0.7px 0 0 blue, -0.7px 0 0 lime;
  }
  27% {
    text-shadow: 0.7px 0 0 blue, -0.7px 0 0 lime;
  }
  28% {
    text-shadow: 0.7px 0 0 blue, -0.7px 0 0 lime;
  }
  29% {
    text-shadow: 0.7px 0 0 blue, -0.7px 0 0 lime;
  }
  30% {
    text-shadow: 0.7px 0 0 blue, -0.7px 0 0 lime;
  }
  31% {
    text-shadow: -1px 0 0 blue, 1px 0 0 lime;
  }
  32% {
    text-shadow: -1px 0 0 blue, 1px 0 0 lime;
  }
  33% {
    text-shadow: -1px 0 0 blue, 1px 0 0 lime;
  }
  34% {
    text-shadow: -1px 0 0 blue, 1px 0 0 lime;
  }
  35% {
    text-shadow: -1px 0 0 blue, 1px 0 0 lime;
  }
  36% {
    text-shadow: -1px 0 0 blue, 1px 0 0 lime;
  }
  37% {
    text-shadow: -1px 0 0 blue, 1px 0 0 lime;
  }
  38% {
    text-shadow: -1px 0 0 blue, 1px 0 0 lime;
  }
  39% {
    text-shadow: -1px 0 0 blue, 1px 0 0 lime;
  }
  40% {
    text-shadow: -1px 0 0 blue, 1px 0 0 lime;
  }
  41% {
    text-shadow: 50px 0 0 blue, -50px 0 0 lime;
  }
  42% {
    text-shadow: 0 0 0 blue, 0 0 0 lime;
  }
  43% {
    text-shadow: 0.5px 0 0 red, -0.5px 0 0 lime;
  }
  44% {
    text-shadow: 0.5px 0 0 red, -0.5px 0 0 lime;
  }
  45% {
    text-shadow: 0.5px 0 0 red, -0.5px 0 0 lime;
  }
  46% {
    text-shadow: 0.5px 0 0 red, -0.5px 0 0 lime;
  }
  47% {
    text-shadow: -1px 0 0 red, 1px 0 0 lime;
  }
  48% {
    text-shadow: -1px 0 0 red, 1px 0 0 lime;
  }
  49% {
    text-shadow: -1px 0 0 red, 1px 0 0 lime;
  }
  50% {
    text-shadow: -1px 0 0 red, 1px 0 0 lime;
  }
  51% {
    text-shadow: 1px 0 0 red, -1px 0 0 blue;
  }
  52% {
    text-shadow: 1px 0 0 red, -1px 0 0 blue;
  }
  53% {
    text-shadow: 1px 0 0 red, -1px 0 0 blue;
  }
  54% {
    text-shadow: 1px 0 0 red, -1px 0 0 blue;
  }
  55% {
    text-shadow: 1px 0 0 red, -1px 0 0 blue;
  }
  56% {
    text-shadow: -1px 0 0 red, 1px 0 0 blue;
  }
  57% {
    text-shadow: -1px 0 0 red, 1px 0 0 blue;
  }
  58% {
    text-shadow: -1px 0 0 red, 1px 0 0 blue;
  }
  59% {
    text-shadow: -1px 0 0 red, 1px 0 0 blue;
  }
  60% {
    text-shadow: -1px 0 0 red, 1px 0 0 blue;
  }
  61% {
    text-shadow: 30px 0 0 red, -30px 0 0 lime;
  }
  62% {
    text-shadow: 0 0 0 red, 0 0 0 lime;
  }
  63% {
    text-shadow: 0.5px 0 0 red, -0.5px 0 0 blue;
  }
  64% {
    text-shadow: 0.5px 0 0 red, -0.5px 0 0 blue;
  }
  65% {
    text-shadow: 0.5px 0 0 red, -0.5px 0 0 blue;
  }
  66% {
    text-shadow: 0.5px 0 0 red, -0.5px 0 0 blue;
  }
  67% {
    text-shadow: -1px 0 0 red, 1px 0 0 blue;
  }
  68% {
    text-shadow: -1px 0 0 red, 1px 0 0 blue;
  }
  69% {
    text-shadow: -1px 0 0 red, 1px 0 0 blue;
  }
  70% {
    text-shadow: -1px 0 0 red, 1px 0 0 blue;
  }
  71% {
    text-shadow: 70px 0 0 red, -70px 0 0 blue;
  }
  72% {
    text-shadow: 0 0 0 red, 0 0 0 blue;
  }
  73% {
    text-shadow: 1px 0 0 red, -1px 0 0 blue;
  }
  74% {
    text-shadow: 1px 0 0 red, -1px 0 0 blue;
  }
  75% {
    text-shadow: 1px 0 0 red, -1px 0 0 blue;
  }
  76% {
    text-shadow: 1px 0 0 red, -1px 0 0 blue;
  }
  77% {
    text-shadow: -1px 0 0 red, 1px 0 0 blue;
  }
  78% {
    text-shadow: -1px 0 0 red, 1px 0 0 blue;
  }
  79% {
    text-shadow: -1px 0 0 red, 1px 0 0 blue;
  }
  80% {
    text-shadow: -1px 0 0 red, 1px 0 0 blue;
  }
  81% {
    text-shadow: 0.5px 0 0 red, -0.5px 0 0 lime;
  }
  82% {
    text-shadow: 0.5px 0 0 red, -0.5px 0 0 lime;
  }
  83% {
    text-shadow: 0.5px 0 0 red, -0.5px 0 0 lime;
  }
  84% {
    text-shadow: 0.5px 0 0 red, -0.5px 0 0 lime;
  }
  85% {
    text-shadow: 0.5px 0 0 red, -0.5px 0 0 lime;
  }
  86% {
    text-shadow: -1px 0 0 red, 1px 0 0 lime;
  }
  87% {
    text-shadow: -1px 0 0 red, 1px 0 0 lime;
  }
  88% {
    text-shadow: -1px 0 0 red, 1px 0 0 lime;
  }
  89% {
    text-shadow: -1px 0 0 red, 1px 0 0 lime;
  }
  90% {
    text-shadow: -1px 0 0 red, 1px 0 0 lime;
  }
  91% {
    text-shadow: 60px 0 0 lime, -60px 0 0 blue;
  }
  92% {
    text-shadow: 0 0 0 lime, 0 0 0 blue;
  }
  92% {
    text-shadow: 0.7px 0 0 blue, -0.7px 0 0 lime;
  }
  93% {
    text-shadow: 0.7px 0 0 blue, -0.7px 0 0 lime;
  }
  94% {
    text-shadow: 0.7px 0 0 blue, -0.7px 0 0 lime;
  }
  95% {
    text-shadow: 0.7px 0 0 blue, -0.7px 0 0 lime;
  }
  96% {
    text-shadow: -1px 0 0 blue, 1px 0 0 lime;
  }
  97% {
    text-shadow: -1px 0 0 blue, 1px 0 0 lime;
  }
  98% {
    text-shadow: -1px 0 0 blue, 1px 0 0 lime;
  }
  99% {
    text-shadow: -1px 0 0 blue, 1px 0 0 lime;
  }
  100% {
    text-shadow: -1px 0 0 blue, 1px 0 0 lime;
  }
}

</style>

<h1>Securing healthy funding for <span class="glitch">cybersecurity</span> research in healthcare</h1>

Workshop @ NR 6.11.2023

---


## find && who

<div class="halfsplit">
<div class="gridleft"> 

![pres-qr](healthy-cs-funding.png)
https://lassegs.gitlab.io/healty-cs-funding/

</div>
<div class="gridright">

**Lasse Gullvåg Sætre**

[Enabling technologies](https://www.forskningsradet.no/portefoljer/muliggjorende-teknologier/)

lgs@rcn.no

[Forskningsrådet](forskningsradet.no)

NCP Horizon CL3 & DIGITAL, NCC-NO, ++

</div>
</div>


---

<!-- .slide: data-background="https://anti-brands.fra1.digitaloceanspaces.com/brands/forskningsradet/media/_1200x800_fit_center-center_82_none/Forskningsradet_Temabilde_Arrangementer_TeknologiDigitalisering_01_@1x_RGB.jpg" data-background-opacity="0.33"-->

## ls

1. write
2. history
3. nmap
4. curl 

---

<section data-preload >

##  write

<iframe data-src="https://cryptpad.fr/pad/#/2/pad/edit/RPCK1yF0tH8d0Nl5Ys8dbMnu/embed/" class="r-stretch"></iframe>

---

<!-- .slide: data-background="fr-sikkerhet-graf.png" data-background-opacity="0.75"-->

## history

<small>

| 2015                               | 2018                     | 2020-2022                                                    |
|------------------------------------|--------------------------|--------------------------------------------------------------|
| Build disciplines and environments | Apply knowledge          | Continuity and multidisciplinarity                           |
|                                    | Critical infrastructures | KSP Digital Sikkerhet, Off/nærPHD, Innovasjonsprosjekt       |
|                                    |                          | SFI NORCICS 100 mill.                                        |
|                                    |                          | Two large multidisciplinary projects, 50 mill. (IKT+SAMRISK) |
|                                    |                          | Energy, Health, Transport, ++                                |
|                                    |                          | High success rate in H2020                                   |

</small>

---

## nmap

<div class="halfsplit">
<div class="gridleft" style="background:#fff;"> 

- IKTPLUSS 
- AI and digital security, [stakeholder consultations](https://www.forskningsradet.no/arrangementer/2023/ki-innspillsmote-2/) 
- NCC-NO 
- Helse 

</div>
<div class="gridright">

![](https://i.imgur.com/ruVjmQ0.png)

</div>
</div>

Note:

These are budgetary lines, sources of finance. Will be called different things when they come out as calls.
- IKTPLUSS: Future oriented, sustainable, private and secure ICTs 
- NCC: TBD, high TRL / deployment, ++


---

## curl

<small>
<ul>
<li>European Commission, <a href="https://ec.europa.eu/growth/sectors/electrical-and-electronic-engineering-industries-eei/radio-equipment-directive-red_en" target="_blank" rel="noopener">delegated act to the Radio Equipment Directive</a>, 29 October 2021</li>
<li>European Commission, <a href="https://ec.europa.eu/digital-single-market/en/cybersecurity-strategy" target="_blank" rel="noopener">The EU's Cybersecurity Strategy for the Digital Decade</a>, JOIN(2020) 18</li>
<li>European Commission, <a href="https://ec.europa.eu/digital-single-market/news-redirect/696976" target="_blank" rel="noopener">Directive on measures for high common level of cybersecurity across the Union</a><span><span>&nbsp;</span>(revised NIS Directive or &lsquo;NIS 2'),COM(2020) 823</span></li>
<li><span>European Commission,&nbsp;<a href="https://ec.europa.eu/home-affairs/sites/homeaffairs/files/pdf/15122020_proposal_directive_resilience_critical_entities_com-2020-829_en.pdf" target="_blank" rel="noopener">Directive on the resilience of critical entities</a><span>, </span>COM(2020) 829 </span></li>
<li>European Commission, Proposal for a <a href="http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1505737096808&amp;uri=CELEX:52017PC0477" target="_blank" rel="noopener">Regulation on ENISA, the "EU Cybersecurity Agency", and repealing Regulation (EU) 526/2013, and on Information and Communication Technology cybersecurity certification (''Cybersecurity Act'')</a> 2017/0225(COD)</li>
<li>European Parliament, <a href="http://www.europarl.europa.eu/thinktank/en/document.html?reference=EPRS_BRI(2021)689333" target="_blank" rel="noopener">The NIS2 Directive: A high common level of cybersecurity in the EU</a>, legislative briefing, EPRS, November, 2021.</li>
<li>European Parliament, <a href="https://www.europarl.europa.eu/RegData/etudes/BRIE/2020/654198/EPRS_BRI(2020)654198_EN.pdf" target="_blank" rel="noopener">Directive on security of network and information systems (NIS Directive)</a>, Implementation appraisal briefing, EPRS, November 2020.</li>
<li>European Parliament, <a href="https://www.europarl.europa.eu/RegData/etudes/BRIE/2021/662606/EPRS_BRI(2021)662606_EN.pdf" target="_blank" rel="noopener">Improving the common level of cybersecurity across the EU</a>, Initial Appraisal of a European Commission Impact Assessment, EPRS , February 2021</li>
<li>European Council, <a href="https://data.consilium.europa.eu/doc/document/ST-6722-2021-INIT/en/pdf" target="_blank" rel="noopener">Conclusions on the EU cybersecurity strategy</a>,&nbsp;22 March 2021</li>
<li>European Commission<span>&nbsp;</span><a href="https://digital-strategy.ec.europa.eu/en/library/communication-cybersecurity-skills-academy" target="_blank" rel="noopener">Communication on the Cybersecurity Skills Academy COM(2023)207</a>,&nbsp;</li>
</ul>
</small>

Note:

Taken from Maria del Mar Negreiro Achiaga, Members' Research Service, legislative-train@europarl.europa.eu

---

## curl(2)

| Program | Topic                                                                                 | Budget |
|---------|---------------------------------------------------------------------------------------|--------|
| HEU CL3 | Approaches and tools for security in software and hardware development and assessment | €37M   |
| HEU CL3 | Post-quantum cryptography transition                                                  | €23M   |

---

## curl(3)

<img class="r-stretch" src="proposed-dep-cs.png">

---

<!-- .slide: data-background-video="https://anti-brands.fra1.digitaloceanspaces.com/brands/forskningsradet/media/NFR_INTROPOSTER_LONG_NAMETAG_2022-05-05-074407_pwrq.mp4" data-background-opacity="1"-->
